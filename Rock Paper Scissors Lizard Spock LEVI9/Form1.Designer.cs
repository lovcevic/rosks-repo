﻿namespace Rock_Paper_Scissors_Lizard_Spock_LEVI9
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lblPc = new System.Windows.Forms.Label();
            this.lblPlayer = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.lblStatus = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.pbSpockPC = new System.Windows.Forms.PictureBox();
            this.pbLizardPC = new System.Windows.Forms.PictureBox();
            this.pbSpock = new System.Windows.Forms.PictureBox();
            this.pbLizard = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pbScissorsPC = new System.Windows.Forms.PictureBox();
            this.pbPaperPC = new System.Windows.Forms.PictureBox();
            this.pbRockPC = new System.Windows.Forms.PictureBox();
            this.pbScissors = new System.Windows.Forms.PictureBox();
            this.pbPaper = new System.Windows.Forms.PictureBox();
            this.pbRock = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbSpockPC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLizardPC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSpock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLizard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbScissorsPC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPaperPC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRockPC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbScissors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPaper)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRock)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Showcard Gothic", 20F);
            this.label1.Location = new System.Drawing.Point(1015, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 33);
            this.label1.TabIndex = 7;
            this.label1.Text = "SCORE:";
            // 
            // lblPc
            // 
            this.lblPc.AutoSize = true;
            this.lblPc.Font = new System.Drawing.Font("Showcard Gothic", 17F);
            this.lblPc.Location = new System.Drawing.Point(964, 114);
            this.lblPc.Name = "lblPc";
            this.lblPc.Size = new System.Drawing.Size(78, 29);
            this.lblPc.TabIndex = 8;
            this.lblPc.Text = "PC = 0";
            // 
            // lblPlayer
            // 
            this.lblPlayer.AutoSize = true;
            this.lblPlayer.Font = new System.Drawing.Font("Showcard Gothic", 17F);
            this.lblPlayer.Location = new System.Drawing.Point(964, 149);
            this.lblPlayer.Name = "lblPlayer";
            this.lblPlayer.Size = new System.Drawing.Size(135, 29);
            this.lblPlayer.TabIndex = 9;
            this.lblPlayer.Text = "PLAYER = 0";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button1.Font = new System.Drawing.Font("Showcard Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button1.Location = new System.Drawing.Point(959, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(200, 36);
            this.button1.TabIndex = 10;
            this.button1.Text = "HOW TO PLAY";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button2.Font = new System.Drawing.Font("Showcard Gothic", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(959, 253);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(200, 188);
            this.button2.TabIndex = 18;
            this.button2.Text = "NEW TRY";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Showcard Gothic", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Location = new System.Drawing.Point(367, 253);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 50);
            this.lblStatus.TabIndex = 19;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button3.Font = new System.Drawing.Font("Showcard Gothic", 15F);
            this.button3.Location = new System.Drawing.Point(959, 466);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(200, 35);
            this.button3.TabIndex = 20;
            this.button3.Text = "RESET SCORE";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // pbSpockPC
            // 
            this.pbSpockPC.Cursor = System.Windows.Forms.Cursors.No;
            this.pbSpockPC.Image = global::Rock_Paper_Scissors_Lizard_Spock_LEVI9.Properties.Resources.Spock;
            this.pbSpockPC.Location = new System.Drawing.Point(743, 106);
            this.pbSpockPC.Name = "pbSpockPC";
            this.pbSpockPC.Size = new System.Drawing.Size(100, 104);
            this.pbSpockPC.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbSpockPC.TabIndex = 24;
            this.pbSpockPC.TabStop = false;
            this.pbSpockPC.Tag = "";
            // 
            // pbLizardPC
            // 
            this.pbLizardPC.Cursor = System.Windows.Forms.Cursors.No;
            this.pbLizardPC.Image = global::Rock_Paper_Scissors_Lizard_Spock_LEVI9.Properties.Resources.Lizard;
            this.pbLizardPC.Location = new System.Drawing.Point(577, 106);
            this.pbLizardPC.Name = "pbLizardPC";
            this.pbLizardPC.Size = new System.Drawing.Size(100, 104);
            this.pbLizardPC.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbLizardPC.TabIndex = 23;
            this.pbLizardPC.TabStop = false;
            this.pbLizardPC.Tag = "";
            // 
            // pbSpock
            // 
            this.pbSpock.Image = global::Rock_Paper_Scissors_Lizard_Spock_LEVI9.Properties.Resources.Spock;
            this.pbSpock.Location = new System.Drawing.Point(743, 337);
            this.pbSpock.Name = "pbSpock";
            this.pbSpock.Size = new System.Drawing.Size(100, 104);
            this.pbSpock.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbSpock.TabIndex = 22;
            this.pbSpock.TabStop = false;
            this.pbSpock.Click += new System.EventHandler(this.pbSpock_Click);
            // 
            // pbLizard
            // 
            this.pbLizard.Image = global::Rock_Paper_Scissors_Lizard_Spock_LEVI9.Properties.Resources.Lizard;
            this.pbLizard.Location = new System.Drawing.Point(577, 337);
            this.pbLizard.Name = "pbLizard";
            this.pbLizard.Size = new System.Drawing.Size(100, 104);
            this.pbLizard.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbLizard.TabIndex = 21;
            this.pbLizard.TabStop = false;
            this.pbLizard.Click += new System.EventHandler(this.pbLizard_Click);
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = global::Rock_Paper_Scissors_Lizard_Spock_LEVI9.Properties.Resources.Human_D_512;
            this.pictureBox9.Location = new System.Drawing.Point(397, 466);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(100, 74);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox9.TabIndex = 17;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Cursor = System.Windows.Forms.Cursors.No;
            this.pictureBox8.Image = global::Rock_Paper_Scissors_Lizard_Spock_LEVI9.Properties.Resources.Icon_pc;
            this.pictureBox8.Location = new System.Drawing.Point(397, 12);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(100, 74);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 16;
            this.pictureBox8.TabStop = false;
            // 
            // pbScissorsPC
            // 
            this.pbScissorsPC.Cursor = System.Windows.Forms.Cursors.No;
            this.pbScissorsPC.Image = global::Rock_Paper_Scissors_Lizard_Spock_LEVI9.Properties.Resources.Scissors;
            this.pbScissorsPC.Location = new System.Drawing.Point(397, 106);
            this.pbScissorsPC.Name = "pbScissorsPC";
            this.pbScissorsPC.Size = new System.Drawing.Size(100, 104);
            this.pbScissorsPC.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbScissorsPC.TabIndex = 13;
            this.pbScissorsPC.TabStop = false;
            this.pbScissorsPC.Tag = "";
            // 
            // pbPaperPC
            // 
            this.pbPaperPC.Cursor = System.Windows.Forms.Cursors.No;
            this.pbPaperPC.Image = global::Rock_Paper_Scissors_Lizard_Spock_LEVI9.Properties.Resources.paper;
            this.pbPaperPC.Location = new System.Drawing.Point(224, 106);
            this.pbPaperPC.Name = "pbPaperPC";
            this.pbPaperPC.Size = new System.Drawing.Size(100, 104);
            this.pbPaperPC.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbPaperPC.TabIndex = 12;
            this.pbPaperPC.TabStop = false;
            this.pbPaperPC.Tag = "";
            // 
            // pbRockPC
            // 
            this.pbRockPC.Cursor = System.Windows.Forms.Cursors.No;
            this.pbRockPC.Image = global::Rock_Paper_Scissors_Lizard_Spock_LEVI9.Properties.Resources.Rock;
            this.pbRockPC.Location = new System.Drawing.Point(50, 106);
            this.pbRockPC.Name = "pbRockPC";
            this.pbRockPC.Size = new System.Drawing.Size(100, 104);
            this.pbRockPC.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbRockPC.TabIndex = 11;
            this.pbRockPC.TabStop = false;
            this.pbRockPC.Tag = "";
            // 
            // pbScissors
            // 
            this.pbScissors.Image = global::Rock_Paper_Scissors_Lizard_Spock_LEVI9.Properties.Resources.Scissors;
            this.pbScissors.Location = new System.Drawing.Point(397, 337);
            this.pbScissors.Name = "pbScissors";
            this.pbScissors.Size = new System.Drawing.Size(100, 104);
            this.pbScissors.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbScissors.TabIndex = 2;
            this.pbScissors.TabStop = false;
            this.pbScissors.Click += new System.EventHandler(this.pbScissors_Click);
            // 
            // pbPaper
            // 
            this.pbPaper.Image = global::Rock_Paper_Scissors_Lizard_Spock_LEVI9.Properties.Resources.paper;
            this.pbPaper.Location = new System.Drawing.Point(224, 337);
            this.pbPaper.Name = "pbPaper";
            this.pbPaper.Size = new System.Drawing.Size(100, 104);
            this.pbPaper.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbPaper.TabIndex = 1;
            this.pbPaper.TabStop = false;
            this.pbPaper.Click += new System.EventHandler(this.pbPaper_Click);
            // 
            // pbRock
            // 
            this.pbRock.Image = global::Rock_Paper_Scissors_Lizard_Spock_LEVI9.Properties.Resources.Rock;
            this.pbRock.Location = new System.Drawing.Point(50, 337);
            this.pbRock.Name = "pbRock";
            this.pbRock.Size = new System.Drawing.Size(100, 104);
            this.pbRock.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbRock.TabIndex = 0;
            this.pbRock.TabStop = false;
            this.pbRock.Click += new System.EventHandler(this.pbRock_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1184, 556);
            this.Controls.Add(this.pbSpockPC);
            this.Controls.Add(this.pbLizardPC);
            this.Controls.Add(this.pbSpock);
            this.Controls.Add(this.pbLizard);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pbScissorsPC);
            this.Controls.Add(this.pbPaperPC);
            this.Controls.Add(this.pbRockPC);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lblPlayer);
            this.Controls.Add(this.lblPc);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pbScissors);
            this.Controls.Add(this.pbPaper);
            this.Controls.Add(this.pbRock);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1200, 595);
            this.MinimumSize = new System.Drawing.Size(1200, 595);
            this.Name = "Form1";
            this.Text = "ROCK PAPER SCISSSORS";
            ((System.ComponentModel.ISupportInitialize)(this.pbSpockPC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLizardPC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSpock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLizard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbScissorsPC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPaperPC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRockPC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbScissors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPaper)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRock)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbRock;
        private System.Windows.Forms.PictureBox pbPaper;
        private System.Windows.Forms.PictureBox pbScissors;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblPc;
        private System.Windows.Forms.Label lblPlayer;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pbScissorsPC;
        private System.Windows.Forms.PictureBox pbPaperPC;
        private System.Windows.Forms.PictureBox pbRockPC;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.PictureBox pbSpockPC;
        private System.Windows.Forms.PictureBox pbLizardPC;
        private System.Windows.Forms.PictureBox pbSpock;
        private System.Windows.Forms.PictureBox pbLizard;
    }
}

