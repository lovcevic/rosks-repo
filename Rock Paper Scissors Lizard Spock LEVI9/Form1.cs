﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//Mladen Lovčević 
//Tehnički fakultet Mihajlo Pupin

namespace Rock_Paper_Scissors_Lizard_Spock_LEVI9
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int pc = 0;
        int player = 0;

        private void pbRock_Click(object sender, EventArgs e)
        {
            pbRock.Enabled = false;

            pbPaper.Hide();
            pbScissors.Hide();
            pbLizard.Hide();
            pbSpock.Hide();

            Random random = new Random((int)DateTime.Now.Ticks);
            int randomValue = random.Next(0, 5); //(startValue, endValue + 1);

            if (randomValue == 0) //rock
            {
                pbPaperPC.Hide();
                pbScissorsPC.Hide();
                pbLizardPC.Hide();
                pbSpockPC.Hide();
                lblStatus.Text = "DRAW";
            }

            if (randomValue == 1)//paper
            {
                pbRockPC.Hide();
                pbScissorsPC.Hide();
                pbLizardPC.Hide();
                pbSpockPC.Hide();
                pc++;
                lblPc.Text = "PC= " + pc;
                lblStatus.Text = "PC WINS";
            }

            if (randomValue == 2)//scissors
            {
                pbPaperPC.Hide();
                pbRockPC.Hide();
                pbLizardPC.Hide();
                pbSpockPC.Hide();
                player++;
                lblPlayer.Text = "Player= " + player;
                lblStatus.Text = "PLAYER WINS";
            }
            if (randomValue == 3)//lizard
            {
                pbPaperPC.Hide();
                pbRockPC.Hide();
                pbScissorsPC.Hide();
                pbSpockPC.Hide();
                player++;
                lblPlayer.Text = "Player= " + player;
                lblStatus.Text = "PLAYER WINS";
            }
            if (randomValue == 4)//spock
            {
                pbPaperPC.Hide();
                pbRockPC.Hide();
                pbScissorsPC.Hide();
                pbLizardPC.Hide();
                pc++;
                lblPc.Text = "PC= " + pc;
                lblStatus.Text = "PC WINS";
            }

        }

        private void pbPaper_Click(object sender, EventArgs e)
        {
            pbPaper.Enabled = false;

            pbRock.Hide();
            pbScissors.Hide();
            pbLizard.Hide();
            pbSpock.Hide();

            Random random = new Random((int)DateTime.Now.Ticks);
            int randomValue = random.Next(0, 5); //(startValue, endValue + 1);

            if (randomValue == 0) //rock
            {
                pbPaperPC.Hide();
                pbScissorsPC.Hide();
                pbLizardPC.Hide();
                pbSpockPC.Hide();
                player++;
                lblPlayer.Text = "Player= " + player;
                lblStatus.Text = "PLAYER WINS";
            }

            if (randomValue == 1)//paper
            {
                pbRockPC.Hide();
                pbScissorsPC.Hide();
                pbLizardPC.Hide();
                pbSpockPC.Hide();
                lblStatus.Text = "DRAW";
            }

            if (randomValue == 2)//scissors
            {
                pbPaperPC.Hide();
                pbRockPC.Hide();
                pbLizardPC.Hide();
                pbSpockPC.Hide();
                pc++;
                lblPc.Text = "PC= " + pc;
                lblStatus.Text = "PC WINS";
            }
            if (randomValue == 3)//lizard
            {
                pbPaperPC.Hide();
                pbRockPC.Hide();
                pbScissorsPC.Hide();
                pbSpockPC.Hide();
                pc++;
                lblPc.Text = "PC= " + pc;
                lblStatus.Text = "PC WINS";
            }
            if (randomValue == 4)//spock
            {
                pbPaperPC.Hide();
                pbRockPC.Hide();
                pbScissorsPC.Hide();
                pbLizardPC.Hide();
                player++;
                lblPlayer.Text = "Player= " + player;
                lblStatus.Text = "PLAYER WINS";
            }
        }

        private void pbScissors_Click(object sender, EventArgs e)
        {
            pbScissors.Enabled = false;

            pbRock.Hide();
            pbPaper.Hide();
            pbLizard.Hide();
            pbSpock.Hide();

            Random random = new Random((int)DateTime.Now.Ticks);
            int randomValue = random.Next(0, 5); //(startValue, endValue + 1);

            if (randomValue == 0) //rock
            {
                pbPaperPC.Hide();
                pbScissorsPC.Hide();
                pbLizardPC.Hide();
                pbSpockPC.Hide();
                pc++;
                lblPc.Text = "PC= " + pc;
                lblStatus.Text = "PC WINS";
            }

            if (randomValue == 1)//paper
            {
                pbRockPC.Hide();
                pbScissorsPC.Hide();
                pbLizardPC.Hide();
                pbSpockPC.Hide();
                player++;
                lblPlayer.Text = "Player= " + player;
                lblStatus.Text = "PLAYER WINS";
            }

            if (randomValue == 2)//scissors
            {
                pbPaperPC.Hide();
                pbRockPC.Hide();
                pbLizardPC.Hide();
                pbSpockPC.Hide();
                lblStatus.Text = "DRAW";
            }
            if (randomValue == 3)//lizard
            {
                pbPaperPC.Hide();
                pbRockPC.Hide();
                pbScissorsPC.Hide();
                pbSpockPC.Hide();
                player++;
                lblPlayer.Text = "Player= " + player;
                lblStatus.Text = "PLAYER WINS";
            }
            if (randomValue == 4)//spock
            {
                pbPaperPC.Hide();
                pbRockPC.Hide();
                pbScissorsPC.Hide();
                pbLizardPC.Hide();
                pc++;
                lblPc.Text = "PC= " + pc;
                lblStatus.Text = "PC WINS";
            }
        }
        private void pbLizard_Click(object sender, EventArgs e)
        {
            pbLizard.Enabled = false;

            pbRock.Hide();
            pbPaper.Hide();
            pbScissors.Hide();
            pbSpock.Hide();

            Random random = new Random((int)DateTime.Now.Ticks);
            int randomValue = random.Next(0, 5); //(startValue, endValue + 1);

            if (randomValue == 0) //rock
            {
                pbPaperPC.Hide();
                pbScissorsPC.Hide();
                pbLizardPC.Hide();
                pbSpockPC.Hide();
                pc++;
                lblPc.Text = "PC= " + pc;
                lblStatus.Text = "PC WINS";
            }

            if (randomValue == 1)//paper
            {
                pbRockPC.Hide();
                pbScissorsPC.Hide();
                pbLizardPC.Hide();
                pbSpockPC.Hide();
                player++;
                lblPlayer.Text = "Player= " + player;
                lblStatus.Text = "PLAYER WINS";
            }

            if (randomValue == 2)//scissors
            {
                pbPaperPC.Hide();
                pbRockPC.Hide();
                pbLizardPC.Hide();
                pbSpockPC.Hide();
                pc++;
                lblPc.Text = "PC= " + pc;
                lblStatus.Text = "PC WINS";
            }
            if (randomValue == 3)//lizard
            {
                pbPaperPC.Hide();
                pbRockPC.Hide();
                pbScissorsPC.Hide();
                pbSpockPC.Hide();
                lblStatus.Text = "DRAW";
            }
            if (randomValue == 4)//spock
            {
                pbPaperPC.Hide();
                pbRockPC.Hide();
                pbScissorsPC.Hide();
                pbLizardPC.Hide();
                player++;
                lblPlayer.Text = "Player= " + player;
                lblStatus.Text = "PLAYER WINS";
            }
        }
        private void pbSpock_Click(object sender, EventArgs e)
        {
            pbSpock.Enabled = false;

            pbRock.Hide();
            pbPaper.Hide();
            pbScissors.Hide();
            pbLizard.Hide();

            Random random = new Random((int)DateTime.Now.Ticks);
            int randomValue = random.Next(0, 5); //(startValue, endValue + 1);

            if (randomValue == 0) //rock
            {
                pbPaperPC.Hide();
                pbScissorsPC.Hide();
                pbLizardPC.Hide();
                pbSpockPC.Hide();
                pc++;
                lblPc.Text = "PC= " + pc;
                lblStatus.Text = "PC WINS";
            }

            if (randomValue == 1)//paper
            {
                pbRockPC.Hide();
                pbScissorsPC.Hide();
                pbLizardPC.Hide();
                pbSpockPC.Hide();
                pc++;
                lblPc.Text = "PC= " + pc;
                lblStatus.Text = "PC WINS";
            }

            if (randomValue == 2)//scissors
            {
                pbPaperPC.Hide();
                pbRockPC.Hide();
                pbLizardPC.Hide();
                pbSpockPC.Hide();
                player++;
                lblPlayer.Text = "Player= " + player;
                lblStatus.Text = "PLAYER WINS";
            }
            if (randomValue == 3)//lizard
            {
                pbPaperPC.Hide();
                pbRockPC.Hide();
                pbScissorsPC.Hide();
                pbSpockPC.Hide();
                player++;
                lblPlayer.Text = "Player= " + player;
                lblStatus.Text = "PLAYER WINS";
            }
            if (randomValue == 4)//spock
            {
                pbPaperPC.Hide();
                pbRockPC.Hide();
                pbScissorsPC.Hide();
                pbLizardPC.Hide();
                lblStatus.Text = "DRAW";
            }
        }
        private void button1_Click(object sender, EventArgs e)//how to play dugme
        {
            HowToPlay form2 = new HowToPlay();
            form2.Tag = this;
            form2.Show(this);
            Hide();
        }

        private void button2_Click(object sender, EventArgs e)//new try dugme
        {
            pbRock.Enabled = true;
            pbPaper.Enabled = true;
            pbScissors.Enabled = true;
            pbLizard.Enabled = true;
            pbSpock.Enabled = true;




            pbRock.Show();
            pbPaper.Show();
            pbScissors.Show();
            pbLizard.Show();
            pbSpock.Show();

            pbRockPC.Show();
            pbPaperPC.Show();
            pbScissorsPC.Show();
            pbLizardPC.Show();
            pbSpockPC.Show();

            lblStatus.Text = "";
        }

        private void button3_Click(object sender, EventArgs e)//reset dugme
        {
            pbRock.Enabled = true;
            pbPaper.Enabled = true;
            pbScissors.Enabled = true;
            pbLizard.Enabled = true;
            pbSpock.Enabled = true;


            pc = 0;
            player = 0;
            lblPc.Text = "PC= 0";
            lblPlayer.Text = "Player= 0";
            lblStatus.Text = "";
            pbRock.Show();
            pbPaper.Show();
            pbScissors.Show();
            pbLizard.Show();
            pbSpock.Show();

            pbRockPC.Show();
            pbPaperPC.Show();
            pbScissorsPC.Show();
            pbLizardPC.Show();
            pbSpockPC.Show();
        }
    }
}

